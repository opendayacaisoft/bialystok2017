package com.acaisoft.openday.security;


import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Service
public class AuthenticationExceptionHandler {

    public void onAuthenticationFailure(HttpServletRequest request, HttpServletResponse response, Exception ex) {
        response.setStatus(HttpServletResponse.SC_FORBIDDEN);
        response.setHeader(HttpHeaders.CONTENT_TYPE, "application/json");

        ex.printStackTrace();
    }
}
