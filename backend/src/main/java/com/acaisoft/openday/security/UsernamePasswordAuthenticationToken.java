package com.acaisoft.openday.security;


import org.springframework.security.web.authentication.preauth.PreAuthenticatedAuthenticationToken;

public class UsernamePasswordAuthenticationToken extends PreAuthenticatedAuthenticationToken {

    private static final long serialVersionUID = -6632057310236870816L;

    public UsernamePasswordAuthenticationToken(Object aPrincipal, Object aCredentials) {
        super(aPrincipal, aCredentials);
    }

}
