package com.acaisoft.openday.views.books.controllers;

import com.acaisoft.openday.db.BookslistDB;
import com.acaisoft.openday.db.BookslistDBException;
import com.acaisoft.openday.schema.Book;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api")
public class BooksController {
    private BookslistDB bookslistDB;

    @Autowired
    public BooksController(BookslistDB bookslistDB) {
        this.bookslistDB = bookslistDB;
    }

    @GetMapping("/bookslist")
    public ResponseEntity<List<Book>> getBooksList() {
        return ResponseEntity.ok(this.bookslistDB.getBooksList());
    }
}
