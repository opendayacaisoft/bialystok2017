package com.acaisoft.openday.views.user.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class UserAuthorizationException extends Exception {

    public UserAuthorizationException(String message) {
        super(message);
    }
}
