package com.acaisoft.openday.db;

import com.acaisoft.openday.schema.Captcha;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Service
public class CaptchaDB {
    private List<Captcha> captchasList;

    public CaptchaDB() {
        this.captchasList = new ArrayList<>();
    }

    public Optional<Captcha> getCaptcha(String formId) {
        return this.captchasList.stream()
            .filter(captcha -> Objects.equals(captcha.getFormId(), formId))
            .findFirst();
    }

    /*
        Saves a captcha to DB
     */
    public synchronized void saveCaptcha(Captcha captcha) throws CaptchaDBException {
        Optional<Captcha> foundCaptchaOpt = this.getCaptcha(captcha.getFormId());

        if (foundCaptchaOpt.isPresent()) {
            throw new CaptchaDBException("Cannot save captcha. Reason: Captcha with id '" + captcha.getFormId() + "' already exists");
        }

        this.captchasList.add(captcha);
    }

    /*
        Checks if provided captcha is valid
     */
    public boolean isCaptchaValid(String formId, String textValue) {
        Optional<Captcha> foundCaptchaOpt = this.captchasList.stream()
            .filter(captcha -> !captcha.isExpired())
            .filter(captcha -> Objects.equals(captcha.getFormId(), formId))
            .filter(captcha -> Objects.equals(captcha.getTextValue(), textValue))
            .findFirst();

        return foundCaptchaOpt.isPresent();
    }

    /*
        Makes captcha invalid for future requests
     */
    public void invalidateCaptcha(String formId) throws CaptchaDBException {
        Optional<Captcha> captcha = this.getCaptcha(formId);

        if (!captcha.isPresent()) {
            throw new CaptchaDBException("Cannot invalidate captcha. Reason: Captcha with id '" + formId + "' doesn't exist");
        }

        captcha.get().setExpired(true);
    }


}
