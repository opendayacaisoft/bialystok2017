package com.acaisoft.openday.db;

import com.acaisoft.openday.schema.Book;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class BookslistDB {
    private List<Book> booksList;

    public BookslistDB() {
        this.booksList = new ArrayList<>();
        this.booksList = BookslistDBMocks.getMocks();
    }

    public List<Book> getBooksList() {
        return this.booksList;
    }
}
