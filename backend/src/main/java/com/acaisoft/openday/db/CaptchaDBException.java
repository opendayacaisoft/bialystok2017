package com.acaisoft.openday.db;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class CaptchaDBException extends Exception {

    public CaptchaDBException(String message) {
        super(message);
    }
}
