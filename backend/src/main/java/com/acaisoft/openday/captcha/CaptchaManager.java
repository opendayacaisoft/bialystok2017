package com.acaisoft.openday.captcha;

import com.acaisoft.openday.schema.Captcha;
import org.springframework.stereotype.Service;

import java.math.BigInteger;
import java.security.SecureRandom;

@Service
public class CaptchaManager {
    private SecureRandom random = new SecureRandom();

    public CaptchaManager() {
    }

    /*
        Generates a new random captcha
     */
    public Captcha generateCaptcha() {
        return new Captcha()
            .setFormId(this.generateRandomString(120))
            .setTextValue(this.generateRandomString(32).toUpperCase())
            .setExpired(false);
    }

    /*
        Generates a random alphanumeric string
     */
    private String generateRandomString(int bitLength) {
        return new BigInteger(bitLength, random).toString(32);
    }
}
