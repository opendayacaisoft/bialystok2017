# Backend

## Zadanie

#### Treść zdania

Wypożyczenie książki.

#### Opis

Celem zadania jest wypożyczenie książki przez użytkownika znajdującej się w bazie danych.

Należy stworzyć endpoint, do którego można odwołać się za pomocą metody `POST` o adresie `/api/bookslist/{bookId}/borrow`.

Jeśli książka zostanie wypożyczona poprawnie zwrócona ma zostać odpowiedź ze statusem `200`.
W przypadku błędu status ma zostać ustalony wedle jego rodzaju.

POWODZENIA


## Zadanie rozszerzone

Dodać nową rolę SUPER_USER. Osoba z rolą SUPER_USER powinna móc wyświelać listę użytkowników 
w systemie oraz móc zmieniać im hasła. 
