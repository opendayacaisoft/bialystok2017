import { Component, OnInit } from "@angular/core";
import { AcaisoftHttpService } from "../../shared/http/acaisoft-http.service";
import { AbstractControl, FormArray, FormControl, FormGroup, Validators } from "@angular/forms";
import { LoggedUserService } from "../../shared/logged-user/logged-user.service";
import { Router } from "@angular/router";
import { NotificationsService } from "angular2-notifications/dist";
import { LoggedUser } from "../../models/logged-user";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  formGroup: FormGroup;

  constructor(private http: AcaisoftHttpService,
              private loggedUserService: LoggedUserService,
              private router: Router,
              private notificationsService: NotificationsService) {
  }

  ngOnInit() {
    this.formGroup = new FormGroup({
      email: new FormControl("", Validators.required),
      password: new FormControl("", Validators.required)
    });
  }

  /*
   This method sends a login request.
   */
  login(): void {
    if (this.formGroup.invalid) {
      this.markControls(this.formGroup, control => control.markAsDirty());
      console.warn("Cannot login, formGroup is invalid!");
      return;
    }

    const loggedUser: LoggedUser = {
      email: this.formGroup.value.email
    };

    this.http.login(this.formGroup.value)
      .subscribe(
        response => {
          console.log("Login success!");
          loggedUser.jwt = response.headers.get("JWT_TOKEN");
          this.loggedUserService.setLoggedUser(loggedUser);
          this.router.navigate(["/bookslist"]);
        },
        error => {
          console.error("Login request failed, reason: ", error);
          this.notificationsService.error("Error", error);
        })

  }

  /*
   This method invokes a callback function with every FormControl instance in a formGroup
   */
  private markControls(form: FormGroup, callback: (control: AbstractControl) => void): void {
    Object.keys(form.controls)
      .forEach(key => {
        callback(form.get(key));
      })
  }
}
