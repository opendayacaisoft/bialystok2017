import { Component, OnInit } from '@angular/core';
import { Book } from "../../models/book";
import { AcaisoftHttpService } from "../../shared/http/acaisoft-http.service";
import { NotificationsService } from "angular2-notifications/dist";

@Component({
  selector: 'app-books-list',
  templateUrl: './books-list.component.html',
  styleUrls: ['./books-list.component.scss']
})
export class BooksListComponent implements OnInit {
  bookslist: Book[] = [];

  constructor(private http: AcaisoftHttpService,
              private notificationsService: NotificationsService) {
  }

  ngOnInit() {
    this.getBookslist();
  }

  getBookslist(): void {
    this.http.getBookslist()
      .subscribe(
        books => this.bookslist = books,
        error => {
          console.error("Failed to get bookslist:", error);
          this.notificationsService.error("Error", error)
        });
  }

  borrowBook(book: Book): void {
    this.http.borrowBook(book.id)
      .subscribe(
        success => {
          book.quantity--;
          console.log("Successfully borrowed a book");
          this.notificationsService.success("Success", `You have successfully borrowed ${book.title} book`);
        },
        error => {
          console.error("Failed to borrow a book:", error);
          this.notificationsService.error("Error", error);
        }
      )
  }
}
