import { Component, OnInit } from '@angular/core';
import { LoggedUserService } from "../../../shared/logged-user/logged-user.service";
import { Router } from "@angular/router";

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {
  constructor(public loggedUserService: LoggedUserService,
              private router: Router) {

  }

  ngOnInit() {
  }

  logout(): void {
    this.loggedUserService.setLoggedUser(null);
    this.router.navigate(["/login"]);
  }
}
