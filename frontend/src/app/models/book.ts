export interface Book {
  id: string,
  imageUrl: string,
  title: string,
  author: string,
  quantity: number
}
