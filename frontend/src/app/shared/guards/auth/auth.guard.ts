import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { LoggedUserService } from "../../logged-user/logged-user.service";

@Injectable()
export class AuthGuard implements CanActivate {
  constructor(private loggedUserService: LoggedUserService,
              private router: Router) {
  }

  canActivate(next: ActivatedRouteSnapshot,
              state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    if (this.loggedUserService.isLoggedUser()) {
      return true;
    }

    this.router.navigate(["/login"]);
    return false;
  }
}
